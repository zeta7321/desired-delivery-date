# Desire delivery date #

![4.9.3](https://img.shields.io/badge/version-4.9.3-green.svg)

Модуль позволяет добавить Desire delivery date 


### Details ###

Данный модуль позволяет добавить настройку желаемой даты доставки.

* Developed for CS-CART EDITION 4.9.3

### What should I check? ###

* Выбор даты в заказе для товара

### Questions? ###

* Developer: Kutyumov Andrey
* Manager: Sergei Minyukevich